const helmet = require('helmet');
const fam = require('./routes/fam');
const home = require('./routes/index');
const genre = require('./routes/genre')
const express = require('express');
const app = express();

app.set('view engine', 'pug');
app.set('views', './views');
app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(helmet());
app.use(express.static('public'));

app.use('/api/v1/sw/fam', fam);
app.use('/', home );
app.use('/genre', genre);

const port = process.env.PORT || 3000;

app.listen(port, (err,res) => {
    if (err) throw err
    console.log(`Execute order 66: http://localhost:${port}`);
});