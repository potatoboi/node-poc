const mongoose = require('mongoose');

// const mongouser = "admin";
// const mongopass = "pass";
// const dbUrl = 'localhost:27017/playground'

// var conUrl = `mongodb://${mongouser}:${mongopass}@${dbUrl}`

mongoose.connect('mongodb://localhost:27017/playground')
    .then(() => console.log('Connected to DB...'))
    .catch(err => console.error("Couldn't connect to DB"));

const userSchema = new mongoose.Schema({
    name: String,
    thing: String,
    tags: [ String],
    date: { type: Date, default: Date.now },
    isJedi: Boolean,
});

const User = mongoose.model('users', userSchema);

async function createUser() {
    const user = new User({
        name: "Luke Skywalker",
        thing: "2",
        tags: ['Chosen', 'One'],
        isJedi: false,
    });
    const result = await user.save();
    console.log(result);
}

async function getUsers() {
    


    const users = await User
    .find()
    .limit(10)
    .sort({ name: 1})
    .select({ name: 1, tags:1 })
    console.log(users);
}

getUsers();
//createUser();