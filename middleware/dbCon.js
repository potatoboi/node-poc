const mysql = require('mysql');
require('dotenv').config();

var dbCon = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
});

// var myq = 'ALTER TABLE movie ADD COLUMN id INT AUTO_INCREMENT PRIMARY KEY';
// dbCon.query(myq,(err,result) => {
//     if (err) throw err
//     console.log(result);
//    })

module.exports = dbCon;