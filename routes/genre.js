const express = require('express');
const router = express.Router();
const dbCon = require('../middleware/dbCon')


router.get('/',(req,res) => {
    var myq = `select * from movie`
    dbCon.query(myq, (err,data) => {
        if (err) throw err
        res.send(JSON.stringify({"status": 200, "error": null, "response": data}));;
    })
})

router.get('/getMovie', (req,res) => {
    var myq = `select * from movie where id=${req.body.id}`
    dbCon.query(myq, (err,data) => {
        if (err) throw err
        res.send(JSON.stringify({"status": 200, "error": null, "response": data}));;
    })
} )

router.post('/', (req,res) => {
    var gname = req.body.name;
    var ggenr = req.body.genre;
    var myq = `INSERT INTO movie (name, genre) VALUES ("${gname}", "${ggenr}")`
    dbCon.query(myq, (err,result) => {
        if (err) throw err
        console.log(result);
    });
    res.send(`Data updated: ${req.body}`)
})

router.put('/', (req,res) => {
    var myq = `UPDATE movie SET name = "${req.body.name}" where id = "${req.body.id}"`
    dbCon.query(myq, (err,res) => {
        if (err) throw err
        console.log(res)
    })
    res.send("Done")
})


router.delete('/', (req,res) => {
    var myq = `DELETE FROM movie where id=${req.body.id}`
    dbCon.query(myq, (err,res) => {
        if (err) throw err
        console.log(res);
    });
    res.send('Data deleted successfully!')
})



module.exports = router;