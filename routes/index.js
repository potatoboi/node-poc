const express = require('express');
const router = express.Router();

router.get('/', (req,res) => {
    // res.send("Welcome earthling!");
    res.render('index', {title: 'PugApp', message: 'Greetings, earthling!'});
});

module.exports = router;