const express = require('express');
const router = express.Router();
const Joi = require('joi');


const users = [
    { id: 1, name: "Yoda"},
    { id: 2, name: "Jin"},
    { id: 3, name: "Obi"},
    { id: 4, name: "Anakin"},
    { id: 5, name: "Luke"},
    { id: 6, name: "Rey"},
];

function validateUser(jedi) {
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(jedi,schema);
}


router.get('/', (req,res) => {
    res.send(users);
});

router.get('/:id', (req,res) => {
    const inp = users.find(c => c.id === parseInt(req.params.id));
    if (!inp) res.status(404).send(`Name with id ${req.params.id} not found`);
    res.send(inp);
});

router.post('/', (req,res) => {
    const { error } = validateUser(req.body);
    if (error) { res.status(400).send(error.message)};

    const newser = {
        id: users.length + 1,
        name: req.body.name,
    };

    users.push(newser);
    res.send(newser);

});

router.put('/:id', (req,res) => {
    const inp = users.find(c=> c.id === parseInt(req.params.id));
    if (!inp) res.status(404).send(`User with id ${req.params.id} not found`);
    const { error } = validateUser(req.body);
    if (error) {
        res.status(400).send(error.message);
        return;
    };

    inp.name = req.body.name;
    res.send(inp);

})




router.delete('/:id', (req,res) => {
    const inp = users.find(c=> c.id === parseInt(req.params.id));
    if (!inp) res.status(404).send(`User with id ${req.params.id} not found`);
    
    const index = users.indexOf(inp);
    users.splice(index,1);
    res.send(inp);

})

module.exports = router;